# vega_board_front

Инструкция по поделючению слайдера owl carousel2.

## Библиотеки

- В ```<head>``` добавляем

```html

<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
```

## JS

- В конец ```<body>``` (после других скриптов) добавляем

```html

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="script.js"></script>
```

- Подкладываем в проект файл **script.js**

## Блоки слайдера

```html

<div class="container">
    <div class="row">

        <div class="col-md-4" id="2022-01-24-16-48-21_1_.left.jpg">
            <div style="height:400px; overflow: hidden;" id="2022-01-24-16-48-21_1_.left.jpg-container">
                <div class="owl-carousel owl-theme">
                    <div class="mySlides" ...>
                        <div class="mySlides" ...>
                        </div>
```

Непосредственно внутри mySlides и описывается каждый кадр(вместе с описанием его дисциплины):

```html

<div class="mySlides">
    <div>
        <a href="https://vega.fcyb.mirea.ru/board/photos/2022-01-24-16-48-21_1_.left.jpg" ...>
    </div>

    <div style="vertical-align: middle; overflow: auto;">
        <span id="2022-01-24-16-48-21_1_.left.jpg-timestamp"
              style="vertical-align: middle;">2022-01-24 16:48:21</span>
    </div>

    <div>
        <b id="2022-01-24-16-48-21_1_.left.jpg-disc">Администрирование Линукс</b> <span
            id="2022-01-24-16-48-21_1_.left.jpg-prep"></span>
        <div class="tooltip1">
            <i>
                <span id="2022-01-24-16-48-21_1_.left.jpg-comment"></span>
            </i>
            <span class="tooltiptext1" id="2022-01-24-16-48-21_1_.left.jpg-commentTooltip">
                                                </span>
        </div>
    </div>

</div>
```

Пример доступен в файле **vega.html**

## Результат:

![](images/readme_example.png)
